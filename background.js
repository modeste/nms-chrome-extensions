chrome.runtime.onInstalled.addListener(async () => {
  /*
  await chrome.storage.sync.set({ "bratuhaMsgList": [
    "salut",
    "comment",
    "salut comment se passe ta journee",
  ] })
  */

  chrome.action.setBadgeText({
    text: 'OFF'
  });
});

const website = 'https://b2b.bratuha.com/controllers/Managers';

chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  if (tab.url.startsWith(website) && changeInfo.status === "complete") {
    let prevState = await chrome.storage.sync.get("bratuhaStageApp")
    prevState = (prevState?.bratuhaStageApp && prevState?.bratuhaStageApp === 'ON') ? 'ON' : 'OFF'

    chrome.action.setBadgeText({
      text: prevState
    });
    // Save state in local storage
    await chrome.storage.sync.set({ "bratuhaStageApp": prevState})

    // execute script here...
    if (prevState === 'ON') {
      chrome.scripting.executeScript({
        target : {tabId },
        func : isActiveScanMsg,
      }).then(() => console.log("injected a function"));
    }
  }
});

// When the user clicks on the extension action
chrome.action.onClicked.addListener(async (tab) => {
  if (tab.url.startsWith(website)) {
    // We retrieve the action badge to check if the extension is 'ON' or 'OFF'
    let prevState = await chrome.storage.sync.get("bratuhaStageApp")
    //const prevState = await chrome.action.getBadgeText({ tabId: tab.id });
    prevState = (prevState?.bratuhaStageApp && prevState?.bratuhaStageApp === 'ON') ? 'ON' : 'OFF'
    // Next state will always be the opposite
    const nextState = prevState === 'ON' ? 'OFF' : 'ON';

    // Set the action badge to the next state
    await chrome.action.setBadgeText({
      tabId: tab.id,
      text: nextState
    });
    await chrome.storage.sync.set({ "bratuhaStageApp": nextState})
 
    if (nextState === 'ON') {
      chrome.scripting.executeScript({
        target : {tabId : tab.id},
        func : isActiveScanMsg,
      }).then(() => console.log("injected a function"));
    } else if (nextState === 'OFF') {
      await chrome.storage.sync.remove("bratuhaMsgList")
    }
  }
});

async function isActiveScanMsg() {
  
  let msgTraitement = ""
  // Get message is typing in live
  const chatInput = document.getElementsByClassName("message_field")[0];
  chatInput.addEventListener("input", async (e) => {
    let x = e.target.value
    // get list messages is storage 
    const checkListMsg = (await chrome.storage.sync.get("bratuhaMsgList"))?.bratuhaMsgList;
     
    // DB keywords of blacklist
    let blacklistKeys = [
      "!!!", "???", "@@@", ",,,", ";;;", ":::", "===", "###", "&&&", "@"
    ]
    // Remove keywork in message
    blacklistKeys.forEach(el => {
      x = x.replaceAll(el, "")
    }); 
    chatInput.value = x 

    msgTraitement = x.replace(/[!"#$%&'()*+,-./:;|<=>{}?@\^_`;]/g, "").trim().toLowerCase()

    if(checkListMsg !== undefined && checkListMsg.includes(msgTraitement)) { 
      // Disable ENTER Touch in keyboard to block message
      let caseInput = document.getElementById('chat_message_field')
      chatInput.removeAttribute('id')
      
      // Insert CSS RED line
      chatInput.style.border = "solid 10px red"
    } else {
      // Enable ENTER Touch in keyboard to send message
      chatInput.setAttribute('id', 'chat_message_field')

      // Remove CSS RED line 
      chatInput.style.border = "inset"
    }
  })

  // When touch Enter in keyboard 
  chatInput.addEventListener("keypress", async (e) => {
    if (e.key === 'Enter') {
      let findListMsg = (await chrome.storage.sync.get("bratuhaMsgList"))?.bratuhaMsgList;
      
      // Save this message in storage 
      if(findListMsg !== undefined && !findListMsg.includes(msgTraitement) && msgTraitement.length) { 
        findListMsg.push(msgTraitement)
      } else {
        findListMsg = [msgTraitement]
      }

      await chrome.storage.sync.set({"bratuhaMsgList": findListMsg })
    }
  })
}
